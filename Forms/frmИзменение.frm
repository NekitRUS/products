VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frm��������� 
   Caption         =   "��������"
   ClientHeight    =   3750
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   6660
   OleObjectBlob   =   "frm���������.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "frm���������"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ���� As String
Public ���� As Workbook
Public �������� As Worksheet
Public ������ As New Collection


Private Sub UserForm_Activate()
       
    On Error GoTo ������
    
    Application.ScreenUpdating = True
    
    Call Module1.������������������
    Call Module1.�������������������
    
    If ������.Count > 0 Then
        cmd��������.Caption = "��������"
        cmd�������.Enabled = True
        cbo�������.ListIndex = 0
    Else
        cmd��������.Caption = "��������"
        cmd�������.Enabled = False
        cbo�������.ListIndex = -1
    End If
    
    Exit Sub
    
������:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbOKOnly + vbCritical, "������!"
    End

End Sub

Private Sub cbo�������_Change()
       
    On Error GoTo ������
    
    For Each ������ In ������
        If cbo�������.Value = ������.�������� Then
            cbo�������.Value = ������.��������
            cbo������.Value = ������.������
            txt����.Value = Replace(������.����, ".", ",")
            txt�����.Value = Replace(������.�����, ".", ",")
            txt����.Value = Replace(������.����, ".", ",")
            txt��������.Value = Replace(������.��������, ".", ",")
            txt����.Value = Replace(������.����, ".", ",")
            cmd��������.Caption = "��������"
            cmd�������.Enabled = True
            Exit Sub
        End If
    Next
    
    If cmd��������.Caption = "��������" Then
        cbo������.Value = ""
        txt����.Value = ""
        txt�����.Value = ""
        txt����.Value = ""
        txt��������.Value = ""
        txt����.Value = ""
    End If
    
    cmd��������.Caption = "��������"
    cmd�������.Enabled = False

    Exit Sub
    
������:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbOKOnly + vbCritical, "������!"
    End

End Sub

Private Sub txt����_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    
    Call Module1.�������������������(KeyAscii)
    
End Sub

Private Sub txt�����_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    
    Call Module1.�������������������(KeyAscii)
    
End Sub

Private Sub txt����_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    
    Call Module1.�������������������(KeyAscii)
    
End Sub

Private Sub txt��������_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    
    Call Module1.�������������������(KeyAscii)
    
End Sub

Private Sub txt����_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    
    Call Module1.�������������������(KeyAscii)
    
End Sub

Private Sub cmd��������_Click()
       
    On Error GoTo ������
    
    If Module1.����������������� = False Then
        cbo�������.SetFocus
        Exit Sub
    End If
    
    Dim ������������ As String
    Dim ����������� As String
    Dim ����������� As Integer
    Dim �������������������� As Integer
    ����������� = LCase(Trim(cbo������.Value))
    
    '��������� ������������ ������
    If cmd��������.Caption = "��������" Then
        ������������ = LCase(������(cbo�������.ListIndex + 1).������)
        '1)...��� �������� � ������ ������ ���������.
        If ������������ = ����������� Then
            With ������(cbo�������.ListIndex + 1)
                .�������� = Trim(cbo�������.Value)
                .���� = CDbl(Trim(txt����.Value))
                .����� = CDbl(Trim(txt�����.Value))
                .���� = CDbl(Trim(txt����.Value))
                .�������� = CDbl(Trim(txt��������.Value))
                .���� = CDbl(Trim(txt����.Value))
            End With
            With ��������
                .Cells(cbo�������.ListIndex + 2, 2).Value = ������(cbo�������.ListIndex + 1).��������
                .Cells(cbo�������.ListIndex + 2, 3).Value = ������(cbo�������.ListIndex + 1).����
                .Cells(cbo�������.ListIndex + 2, 4).Value = ������(cbo�������.ListIndex + 1).�����
                .Cells(cbo�������.ListIndex + 2, 5).Value = ������(cbo�������.ListIndex + 1).����
                .Cells(cbo�������.ListIndex + 2, 6).Value = ������(cbo�������.ListIndex + 1).��������
                .Cells(cbo�������.ListIndex + 2, 7).Value = ������(cbo�������.ListIndex + 1).����
            End With
            Call Module1.�������������������
            cbo�������.SetFocus
        '2)...� ��������� � ������(������������) ������ ���������.
        Else
            Dim ����������������� As ������
            Set ����������������� = New ������
            For ����������� = 2 To ������.Count + 1
                If LCase(��������.Cells(�����������, 1).Value) = ����������� Then
                    With �����������������
                        .�������� = Trim(cbo�������.Value)
                        .������ = UCase(Trim(cbo������.Value))
                        .���� = CDbl(Trim(txt����.Value))
                        .����� = CDbl(Trim(txt�����.Value))
                        .���� = CDbl(Trim(txt����.Value))
                        .�������� = CDbl(Trim(txt��������.Value))
                        .���� = CDbl(Trim(txt����.Value))
                    End With
                    �������������������� = ��������.Cells(�����������, 1).MergeArea.Rows.Count
                    With ��������
                        .Cells(�����������, 1).MergeCells = False
                        .Rows(����������� + ��������������������).Insert Shift:=xlDown
                        .Range(Cells(�����������, 1), Cells(����������� + ��������������������, 1)).MergeCells = True
                        .Cells(����������� + ��������������������, 2).Value = �����������������.��������
                        .Cells(����������� + ��������������������, 3).Value = �����������������.����
                        .Cells(����������� + ��������������������, 4).Value = �����������������.�����
                        .Cells(����������� + ��������������������, 5).Value = �����������������.����
                        .Cells(����������� + ��������������������, 6).Value = �����������������.��������
                        .Cells(����������� + ��������������������, 7).Value = �����������������.����
                    End With
                    ������.Add Item:=�����������������, After:=����������� + �������������������� - 2
                    If ����������� + �������������������� <= cbo�������.ListIndex + 2 Then
                        cbo�������.AddItem ""
                        cbo�������.ListIndex = cbo�������.ListIndex + 1
                    End If
                    Exit For
                End If
            Next �����������
            '3)...� ��������� � ������(����� �����������) ������ ���������.
            If �����������������.�������� = "" Then
                With �����������������
                    .�������� = Trim(cbo�������.Value)
                    .������ = UCase(Trim(cbo������.Value))
                    .���� = CDbl(Trim(txt����.Value))
                    .����� = CDbl(Trim(txt�����.Value))
                    .���� = CDbl(Trim(txt����.Value))
                    .�������� = CDbl(Trim(txt��������.Value))
                    .���� = CDbl(Trim(txt����.Value))
                End With
                With ��������
                    .Cells(������.Count + 2, 1).Value = �����������������.������
                    .Cells(������.Count + 2, 2).Value = �����������������.��������
                    .Cells(������.Count + 2, 3).Value = �����������������.����
                    .Cells(������.Count + 2, 4).Value = �����������������.�����
                    .Cells(������.Count + 2, 5).Value = �����������������.����
                    .Cells(������.Count + 2, 6).Value = �����������������.��������
                    .Cells(������.Count + 2, 7).Value = �����������������.����
                End With
                ������.Add �����������������
            End If
            cmd�������_Click
            cbo�������.Value = �����������������.��������
        End If
    
    '�������� ����� ������
    Else
        Dim ����������� As ������
        Set ����������� = New ������
        '1)...� ������������ ������ ���������.
        For ����������� = 2 To ������.Count + 1
            If LCase(��������.Cells(�����������, 1).Value) = ����������� Then
                With �����������
                    .�������� = Trim(cbo�������.Value)
                    .������ = UCase(Trim(cbo������.Value))
                    .���� = CDbl(Trim(txt����.Value))
                    .����� = CDbl(Trim(txt�����.Value))
                    .���� = CDbl(Trim(txt����.Value))
                    .�������� = CDbl(Trim(txt��������.Value))
                    .���� = CDbl(Trim(txt����.Value))
                End With
                �������������������� = ��������.Cells(�����������, 1).MergeArea.Rows.Count
                With ��������
                    .Cells(�����������, 1).MergeCells = False
                    .Rows(����������� + ��������������������).Insert Shift:=xlDown
                    .Range(Cells(�����������, 1), Cells(����������� + ��������������������, 1)).MergeCells = True
                    .Cells(����������� + ��������������������, 2).Value = �����������.��������
                    .Cells(����������� + ��������������������, 3).Value = �����������.����
                    .Cells(����������� + ��������������������, 4).Value = �����������.�����
                    .Cells(����������� + ��������������������, 5).Value = �����������.����
                    .Cells(����������� + ��������������������, 6).Value = �����������.��������
                    .Cells(����������� + ��������������������, 7).Value = �����������.����
                End With
                ������.Add Item:=�����������, After:=����������� + �������������������� - 2
                Exit For
            End If
        Next �����������
        '2)...� ����� ������ ���������.
        If �����������.�������� = "" Then
            With �����������
                .�������� = Trim(cbo�������.Value)
                .������ = UCase(Trim(cbo������.Value))
                .���� = CDbl(Trim(txt����.Value))
                .����� = CDbl(Trim(txt�����.Value))
                .���� = CDbl(Trim(txt����.Value))
                .�������� = CDbl(Trim(txt��������.Value))
                .���� = CDbl(Trim(txt����.Value))
            End With
            With ��������
                .Cells(������.Count + 2, 1).Value = �����������.������
                .Cells(������.Count + 2, 2).Value = �����������.��������
                .Cells(������.Count + 2, 3).Value = �����������.����
                .Cells(������.Count + 2, 4).Value = �����������.�����
                .Cells(������.Count + 2, 5).Value = �����������.����
                .Cells(������.Count + 2, 6).Value = �����������.��������
                .Cells(������.Count + 2, 7).Value = �����������.����
            End With
            ������.Add �����������
        End If
        Call Module1.�������������������
        Call Module1.�������������������
        cbo�������.Value = �����������.��������
        cbo�������_Change
        cbo�������.SetFocus
        
    End If
    
    Exit Sub
    
������:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbOKOnly + vbCritical, "������!"
    End

End Sub

Private Sub cmd�������_Click()
       
    On Error GoTo ������
    
    Dim ����������� As Integer
    ����������� = cbo�������.ListIndex + 2
    
    Application.DisplayAlerts = False
    
    If ��������.Cells(�����������, 1) = "" Then
        ��������.Rows(�����������).Delete Shift:=xlUp
    ElseIf ��������.Cells(����������� + 1, 1) <> "" Then
        ��������.Rows(�����������).Delete Shift:=xlUp
    ElseIf cbo�������.ListIndex + 1 = ������.Count Then
        ��������.Rows(�����������).Delete Shift:=xlUp
    Else
        Dim �������������������� As Integer
        �������������������� = ��������.Cells(�����������, 1).MergeArea.Rows.Count
        ��������.Cells(�����������, 1).MergeCells = False
        ��������.Cells(����������� + 1, 1).Value = ��������.Cells(�����������, 1).Value
        ��������.Rows(�����������).Delete Shift:=xlUp
        ��������.Range(Cells(�����������, 1), Cells(����������� + �������������������� - 2, 1)).MergeCells = True
    End If
    
    Application.DisplayAlerts = True
        
    ������.Remove cbo�������.ListIndex + 1
    
    Call Module1.�������������������
    
    cbo�������.ListIndex = ����������� - 3
    cbo�������.SetFocus
    
    Call Module1.�������������������
    
    Exit Sub
    
������:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbOKOnly + vbCritical, "������!"
    End

End Sub

Private Sub cmd�����_Click()
    
    End
    
End Sub
